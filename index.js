function testService(events) {
  if (events.length % 2) {
    return false
  }
  const testObj = {}
  return events.every((eve) => {
    if (!testObj[eve[0]]) {
      if (eve[1] === "out") {
        return false
      } else {
        testObj[eve[0]] = eve[1]
        return true
      }
    }
    if (testObj[eve[0]]) {
      if (eve[1] === "out") {
        testObj[eve[0]] = undefined
        return true
      } else {
        return true
      }
    }
  })
}

module.exports = testService
